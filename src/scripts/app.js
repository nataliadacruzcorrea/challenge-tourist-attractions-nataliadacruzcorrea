function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#file_upload') 
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

document.addEventListener("DOMContentLoaded", function() {
    const list = [];

    const form = document.querySelector('.tourist-form');
    const inputTitle = document.querySelector('.input-title');
    const inputDesc = document.querySelector('.input-description');
    const inputImage = document.querySelector('.input-image');
    items = document.querySelector(".attractions-items");

    form.addEventListener('submit', addItemToList);
    

    function addItemToList (event) {
        event.preventDefault();

        const itemTitle = event.target["item-title"].value;
        const itemDescription = event.target["item-description"].value;
        const itemImage = event.target["upfile"].value;

        if (itemTitle, itemDescription != "") {

            const item = {
                title: itemTitle,
                description: itemDescription,
            };

            list.push(item);

            renderListItems();
            resetInputs();
        }
    }

    function renderListItems() {
        let itemsStructure = "";

        list.forEach(function (item) {
            itemsStructure += `
                    <li class="attractions-item">
                    <span class="attractions-item-title">${item.title}</span>
                    <span class="attractions-item-description">${item.description}</span>
                </li>
            `;
        });

        items.innerHTML = itemsStructure;
        
    }

    function resetInputs() {
        inputTitle.value = "";
        inputDesc.value = "";
    }



});







